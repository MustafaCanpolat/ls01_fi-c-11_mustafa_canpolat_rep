
public class TemparaturTabelle {

	public static void main(String[] args) {
		// Aufgabe 1.6 Temparatur Tabelle		
		double fh1 = 20;
		double fh2 = 10;
		double fh3 = 0;
		double fh4 = 20;
		double fh5 = 30;
		double cs1 = 28.8889;
		double cs2 = 23.3333;
		double cs3 = 17.7778;
		double cs4 = 6.6667;
		double cs5 = 1.1111;
		
		System.out.printf("Fahrenheit | %2.0s Celsius  \n" , "|");
		System.out.printf("----------------------- \n");
		System.out.printf("%-1.0f %8s %10.2f \n" + 
		"%-1.0f %9s %10.2f \n" +
		"%-1.0f %10s %10.2f\n" + 
		"%-1.0f %9s %10.2f \n" + 
		"%-1.0f %9s %10.2f", 
		-fh1,"|",-cs1, fh2,"|", -cs2, fh3, "|", -cs3, fh4, "|", -cs4, fh5, "|", -cs5);
	}

}
