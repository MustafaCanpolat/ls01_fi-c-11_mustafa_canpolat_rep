
public class Formatierung {

	public static void main(String[] args) {
		// Aufgabe 1 Beispielsätze und Kommentare
		System.out.println("A 1.5 Aufgabe 1");
		System.out.print("Es sollte mehr Vegane Sachen in der Mensa geben! ");
		System.out.println("Ja! Da stimme ich dir zu.");
		
		// Aufgabe 2 Tannenbaum
		System.out.println("\nA 1.5 Aufgabe 2");
		String s = "*************";
		System.out.printf( "%8.1s\n", s );
		System.out.printf( "%9.3s\n", s );
		System.out.printf( "%10.5s\n", s );
		System.out.printf( "%11.7s\n", s );
		System.out.printf( "%12.9s\n", s );
		System.out.printf( "%13.11s\n", s );
		System.out.printf( "%9.3s\n", s );
		System.out.printf( "%9.3s\n", s );
		
		// Aufgabe 3 2 Nachkommastellen
		System.out.println("\nA 1.5 Aufgabe 3");
		System.out.println("<terminated>Aufgabe3[Java Application]");
		System.out.println("--------------------------------------");
		System.out.printf( "|%.2f| |%.2f|\n" ,22.4234234, 22.4234234);
		System.out.printf( "|%.2f| |%.2f|\n" ,111.2222, 111.2222);
		System.out.printf( "|%.2f| |%.2f|\n" ,4.0, 4.0);
		System.out.printf( "|%.2f| |%.2f|\n" ,1000000.551, 1000000.551);
		System.out.printf( "|%.2f| |%.2f|\n" ,97.34, 97.34);
		
	}

}
